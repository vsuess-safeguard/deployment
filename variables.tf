variable "domain_name" {
  description = "Domain for the whole project."
  type        = string
}

variable "safeguard_sub_domain" {
  description = "Sub domain name for the Safeguard"
  type        = string
}

variable "cognito_sub_domain" {
  description = "Sub domain name for AWS Cognito"
  type        = string
}

variable "hosted_zone_id" {
  description = "The ID of the hosted zone created by amazon for the domain record."
  type        = string
}
resource "aws_acm_certificate" "certificate" {
  provider          = aws.us-east-1
  domain_name       = var.domain_name
  validation_method = "DNS"
  subject_alternative_names = [
    join("", ["*.", var.domain_name])
  ]
}

resource "aws_route53_record" "cert_dns_record" {
  for_each = {
    for dvo in aws_acm_certificate.certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.hosted_zone_id
}

resource "aws_acm_certificate_validation" "cert_validation" {
  provider                = aws.us-east-1
  certificate_arn         = aws_acm_certificate.certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_dns_record : record.fqdn]
}

resource "aws_api_gateway_domain_name" "api_gw_domain_safeguard" {
  provider        = aws.eu-central-1
  domain_name     = join("", [var.safeguard_sub_domain, ".", var.domain_name])
  certificate_arn = aws_acm_certificate.certificate.arn
  security_policy = "TLS_1_2"
}

resource "aws_route53_record" "dns_record" {
  name     = var.safeguard_sub_domain
  type     = "A"
  zone_id  = var.hosted_zone_id
  provider = aws.eu-central-1

  alias {
    evaluate_target_health = false
    name                   = aws_api_gateway_domain_name.api_gw_domain_safeguard.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.api_gw_domain_safeguard.cloudfront_zone_id
  }
}

resource "aws_ssm_parameter" "safeguard_subdomain" {
  name      = "/safeguard/domain"
  type      = "String"
  value     = join("", [var.safeguard_sub_domain, ".", aws_api_gateway_domain_name.api_gw_domain_safeguard.domain_name])
  overwrite = true
}

resource "aws_ssm_parameter" "base_url" {
  name      = "/safeguard/api_url"
  type      = "String"
  value     = join("", ["https://", aws_api_gateway_domain_name.api_gw_domain_safeguard.domain_name])
  overwrite = true
}
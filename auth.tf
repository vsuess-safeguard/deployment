resource "aws_cognito_user_pool" "safeguard_user_pool" {
  name              = "safeguard"
  mfa_configuration = "OFF"
  admin_create_user_config {
    allow_admin_create_user_only = true
  }
  password_policy {
    minimum_length    = 8
    require_lowercase = true
    require_numbers   = true
    require_symbols   = true
    require_uppercase = true
  }
}

resource "aws_cognito_user_pool_client" "safeguard_user_pool_client" {
  name                                 = "safeguard_client"
  user_pool_id                         = aws_cognito_user_pool.safeguard_user_pool.id
  generate_secret                      = false
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows = [
    "code", "implicit" # todo consider to remove implicit flow
  ]
  allowed_oauth_scopes = [
    "email", "openid"
  ]
  explicit_auth_flows = [
    "ALLOW_USER_PASSWORD_AUTH", "ALLOW_CUSTOM_AUTH", "ALLOW_USER_SRP_AUTH", "ALLOW_REFRESH_TOKEN_AUTH"
  ]
  callback_urls = [
    join("", ["https://", var.domain_name, "/api/callback"]),
    join("", ["https://", var.safeguard_sub_domain, ".", var.domain_name, "/api/callback"])
  ]
  supported_identity_providers = [
    "COGNITO"
  ]
}

resource "aws_cognito_user_pool_domain" "safeguard_user_pool_domain" {
  domain          = join("", [var.cognito_sub_domain, ".", var.domain_name])
  user_pool_id    = aws_cognito_user_pool.safeguard_user_pool.id
  certificate_arn = aws_acm_certificate.certificate.arn
}

resource "aws_route53_record" "cognito_dns_record" {
  name     = aws_cognito_user_pool_domain.safeguard_user_pool_domain.domain
  type     = "A"
  zone_id  = var.hosted_zone_id
  provider = aws.eu-central-1

  alias {
    evaluate_target_health = false
    name                   = aws_cognito_user_pool_domain.safeguard_user_pool_domain.cloudfront_distribution_arn
    zone_id                = "Z2FDTNDATAQYW2" # fixed value
  }
}

resource "aws_ssm_parameter" "safeguard_user_pool_arn" {
  name        = "/safeguard/cognito/userpool/arn"
  type        = "String"
  value       = aws_cognito_user_pool.safeguard_user_pool.arn
  description = "AWS Cognito UserPool Arn"
  overwrite   = true
}

resource "aws_ssm_parameter" "safeguard_user_pool_id" {
  name        = "/safeguard/cognito/userpool/id"
  type        = "String"
  value       = aws_cognito_user_pool.safeguard_user_pool.id
  description = "AWS Cognito UserPool ID"
  overwrite   = true
}

resource "aws_ssm_parameter" "safeguard_user_pool_client" {
  name        = "/safeguard/cognito/userpool/clientid"
  type        = "String"
  value       = aws_cognito_user_pool_client.safeguard_user_pool_client.id
  description = "AWS Cognito UserPool client id"
  overwrite   = true
}

resource "aws_ssm_parameter" "safeguard_user_pool_domain" {
  name        = "/safeguard/cognito/userpool/domain"
  type        = "String"
  value       = aws_cognito_user_pool_domain.safeguard_user_pool_domain.domain
  description = "AWS Cognito UserPool domain"
  overwrite   = true
}
